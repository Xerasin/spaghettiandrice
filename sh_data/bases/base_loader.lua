if SERVER then AddCSLuaFile() end
local LOADER = {}

function LOADER:AddCSFolder(DIR)
	local GAMEFIL = GetFiles(DIR .. "/*.lua", "LUA")
	for k,v in pairs( GAMEFIL ) do
		if (GLCLIENT) then
			local className = string.StripExtension(string.GetFileFromFilename(v))
			self:Start(className)
			if _G[self.GlobalName] then
				RunFile(v)
			end
			self:End(className)
		end
	end
end

function LOADER:AddSVFolder(DIR)
	local GAMEFIL = GetFiles(DIR .. "/*.lua", "LUA")
	for k,v in pairs( GAMEFIL ) do
		if (GLSERVER) then
			local className = string.StripExtension(string.GetFileFromFilename(v))
			self:Start(className)
			if _G[self.GlobalName] then
				RunFile(v)
			end
			self:End(className)
		end
	end
end


function LOADER:Start(key)
	self.List = self.List or {}
	if self.BASE then
		_G[self.GlobalName] = table.Copy(self.List[key] or self.BASE)
		_G.loader = self
		_G[self.GlobalName].Class = key
	end
end

function LOADER:End()
	if self.List and _G[self.GlobalName] and type(_G[self.GlobalName]) == "table" and _G[self.GlobalName].Class then
		local key = _G[self.GlobalName].Class
		_G.loader = nil
		self.List[key] = _G[self.GlobalName]
	end
end


function LOADER:LoadBaseFolder(folder, className)
	local GAMEPAT = GAMEMODE.Folder:gsub("gamemodes/","") .. "/gamemode/" .. folder
	if file.Exists(GAMEPAT .. "/" .. className .. ".lua", "LUA") then
		include(GAMEPAT .. "/" .. className .. ".lua")
	end
end

function LOADER:LoadBase(className)
	if SERVER then
		self:LoadBaseFolder("sh_data/" .. self.SubFolder .. "/server", className)
	else
		self:LoadBaseFolder("sh_data/" .. self.SubFolder .. "/client", className)
	end
	self:LoadBaseFolder("sh_data/" .. self.SubFolder .. "/shared", className)
end

function LOADER:Grab(keyName, data)
	if self.List and self.List[keyName] then
		local ClassVar = table.Copy(self.List[keyName])
		ClassVar.Class = keyName
		ClassVar:Init(table.Copy(data))
		return ClassVar
	end
end

function LOADER:Exists(className)
	if self.List and self.List[className] then
		return true
	end

	return false
end

function LOADER:PackTable(classes)
	local out = {}
	for _, v in pairs(classes) do
		if v.GetPackage then
			local pack = v:GetPackage()
			if pack then
				table.insert(out, pack)
			end
		end
	end
	return out
end

function LOADER:UnpackTable(packedClasses)
	local out = {}
	for _,v in pairs(packedClasses) do
		if v.class and v.data then
			table.insert(out, self:Grab(v.class, v.data))
		end
	end
	return out
end

function LOADER:Init()
	List = List or {}
	if not self.BASE then
		return
	end
	if not self.GlobalName then
		self.GlobalName = "NONE"
	end
	if not self.SubFolder then
		self.SubFolder = "none"
	end
	self:AddCSFolder("sh_data/" .. self.SubFolder .. "/client")
	self:AddCSFolder("sh_data/" .. self.SubFolder .. "/shared")
	self:AddSVFolder("sh_data/" .. self.SubFolder .. "/shared")
	self:AddSVFolder("sh_data/" .. self.SubFolder .. "/server")
end

BASELOADER = LOADER