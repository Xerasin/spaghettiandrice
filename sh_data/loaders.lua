function CreateLoader(folderName, globalName, baseTbl, oldTbl)
	if not baseTbl or not BASELOADER then return end

	local NEWLOADER = table.Copy(oldTbl or BASELOADER)
	NEWLOADER.BASE = table.Copy(baseTbl)
	NEWLOADER.GlobalName = globalName
	NEWLOADER.SubFolder = folderName
	NEWLOADER:Init()
	return NEWLOADER
end

local ClassesToLoad = {
	{"meshes", "MESH", "BASEMESH", "Meshes", false}
}

timer.Create("WaitForFilesToLoad", 1, 0, function(id, scope, file)
	if BASELOADER then
		for _, class in next, ClassesToLoad do
			local baseTbl = _G[class[3]]
			if baseTbl and not class[5] then
				class[5] = true
				_G[class[4]] = CreateLoader(class[1], class[2], baseTbl, _G[class[4]])
			end

			if not _G[class[4]] then
				class[5] = false
			end
		end
	end
end)




RunFiles("sh_data/bases/*.lua")

