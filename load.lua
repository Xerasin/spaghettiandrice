module("gladdon", package.seeall)
local currVer = 1.0
if gladdon.Version and gladdon.Version > currVer then return end

Version = currVer

if not file.Exists("gladdon", "DATA") then
    file.CreateDir("gladdon")
end


if not lunajson and file.Exists("includes/modules/lunajson.lua", "LUA") then pcall(require, "lunajson") end
if not base64 then pcall(require, "base64") end

local jsondecode = ((lunajson and lunajson.decode) or util.JSONToTable) -- I hate garry's json
local jsonencode  = ((lunajson and lunajson.encode) or util.TableToJSON)
Addons = Addons or {}

function ReadScopeFile(scopeID, scopeFile, tbl)
    tbl[scopeID] = {}

    if file.Exists(scopeFile, "DATA") then
        local compressed = file.Read(scopeFile, "DATA")
        tbl[scopeID] = jsondecode(util.Decompress(compressed))
        for k,v in pairs(tbl[scopeID]) do
            v[2] = (base64 and base64.decode(v[2]) or v[2])
        end
    end
end

function RunRepository(id, startlua)
    local scope = SERVER and "server" or "client"
    id = tonumber(id)
    if not id then return end

    if not startlua then
        startlua = SERVER and "init.lua" or "cl_init"
    end

    if not file.Exists("gladdon/" .. id, "DATA") then
        file.CreateDir("gladdon/" .. id)
    end
    Addons[id] = {}
    ReadScopeFile(scope, "gladdon/" .. id .. "/" .. scope .. ".dat",  Addons[id])
    http.Fetch(("https://gitlab.com/api/v4/projects/%s/repository/tree?recursive=true"):format(id), function(body, len, headers, code)
        if code >= 200 and code < 300 then
            local json = jsondecode(body)
            Addons[id]["Files"] = {}
            for _, fileData in next, json do
                Addons[id]["Files"][fileData.path] = fileData.id
            end

            if (Addons[id]["Files"][startlua]) then
                RunFile(startlua, id, scope)
            end

            hook.Run("GLAddonLoaded", id, scope)
        end
    end, function(err, ah)
    end, {
        ["PRIVATE-TOKEN"] = "VqmuAef2bksovKsuRF_U"
    })
end


function SaveScope(id, scope)
    if Addons[id] and Addons[id][scope] then
        local output = {}
        for fileName, fileData in next, Addons[id][scope] do
            output[fileName] = {fileData[1], base64.encode(fileData[2])}
        end
        file.Write("gladdon/" .. id .. "/" .. scope .. ".dat", util.Compress(jsonencode(output)))
    end
end


function RunString(id, string, scope, path)
    local prev = {}
    local env = {
        GLADDON = id,
        GLSCOPE = scope,
        GLSERVER = scope == "server",
        GLCLIENT = scope == "client",
        RunFolder = function(folder) return gladdon.RunFolder(folder, id, scope) end,
        RunFile = function(filePath) return gladdon.RunFile(filePath, id, scope) end,
        GetFiles = function(folder) return gladdon.GetFiles(folder, id) end,
        RunFiles = function(folder) return gladdon.RunFiles(folder, id, scope) end,
        RunRepository = gladdon.RunRepository
    }

    local GLAddon = CompileString(string, ("[GLAddon %s, %s] %s:"):format(id, scope, path or ""), true)
    if GLAddon then
        for name, value in next, env do
            prev[name] = _G[name]
            _G[name] = value
        end
        GLAddon()
        for name, value in next, prev do
            _G[name] = value
        end
    end
end


function RunFile(filePath, id, scope)
    if not id and GLADDON then id = GLADDON end
    if not scope and GLSCOPE then scope = GLSCOPE end


    local function Run()
        if Addons[id][scope][filePath] then
            RunString(id, Addons[id][scope][filePath][2], scope, filePath)
            hook.Run("GLFileRan", id, scope, filePath)
        end
    end

    local function GrabAndRun()
        http.Fetch(("https://gitlab.com/api/v4/projects/%s/repository/blobs/%s/raw"):format(id, Addons[id]["Files"][filePath]), function(body, len, headers, code)
            if code >= 200 and code < 300 then
                Addons[id][scope][filePath] = {}
                Addons[id][scope][filePath][1] = Addons[id]["Files"][filePath]
                Addons[id][scope][filePath][2] = body
                SaveScope(id, scope)

                Run()
            end
        end, function(err, ah)
        end, {
            ["PRIVATE-TOKEN"] = "VqmuAef2bksovKsuRF_U"
        })
    end

    if Addons[id] and Addons[id]["Files"] and Addons[id]["Files"][filePath] then
        if Addons[id][scope][filePath] then
            if Addons[id][scope][filePath][1] ~= Addons[id]["Files"][filePath] then
                GrabAndRun()
                return
            else
                Run()
            end
        else
            GrabAndRun()
            return
        end
    end
end

function RunFolder(folder, id, scope)
    if not id and GLADDON then id = GLADDON end
    if not scope and GLSCOPE then scope = GLSCOPE end
    if Addons[id] and Addons[id][scope] then
        for fileName, _ in next, Addons[id][scope] do
            local folderPath = string.GetPathFromFilename(fileName)
            if folderPath == folder then
                RunFile(fileName, id, scope)
            end
        end
    end
end

function RunFiles(fileMatch, id, scope)
    local out = {}
    if Addons[id] and Addons[id]["Files"] then
        for fileName, _ in next, Addons[id]["Files"] do
            if fileMatch == nil or fileMatch == "" or fileName:match("^" .. fileMatch:gsub("*", "[^/]*") .. "$") then
                RunFile(fileName, id, scope)
            end
        end
    end
    return out
end

function GetFiles(fileMatch, id)
    local out = {}
    if Addons[id] and Addons[id]["Files"] then
        for fileName, _ in next, Addons[id]["Files"] do
            if fileMatch == nil or fileMatch == "" or fileName:match("^" .. fileMatch:gsub("*", "[^/]*") .. "$") then
                table.insert(out, fileName)
            end
        end
    end
    return out
end
--[==[
RunRepository("15029800", "init.lua")

hook.Add("GLAddonLoaded", "WaitForLoad", function(id, scope)
    PrintTable(GetFiles("i*.lua", id))
end)

hook.Add("GLAddonLoaded", "WaitForLoad", function(id, scope)
    PrintTable(GetFiles("sh_data/*.lua", id))
end)

]==]
hook.Add("GLAddonLoaded", "WaitForLoad", function(id, scope)

end)

hook.Add("GLFileRan", "Fileran", function(id, scope, filePath)
end)

RunRepository("15029800") -- https://gitlab.com/Xerasin/spaghettiandrice